import './Footer.css';
import React from 'react';
import FooterBanner from '../images/bg-bottom-footer.jpg'

class Footer extends React.Component {
  render() {
    return (
      <div class="footer">
        <div className="content">
          <h1>CÔNG TY TNHH CJ CGV VIETNAM</h1>
          <p>Giấy CNĐKDN: 0303675393, đăng ký lần đầu ngày 31/7/2008, đăng ký thay đổi lần thứ 5 ngày 14/10/2015, cấp bởi Sở KHĐT thành phố Hồ Chí Minh.</p>
          <p></p>
        </div>
        <img src={FooterBanner} alt="logo" />
        <img src={FooterBanner} alt="logo" />
      </div>
    );
  }
}

export default Footer;