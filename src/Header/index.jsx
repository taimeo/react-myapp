import './Header.css';
import React from 'react';
import HeaderLogo from '../images/cgvlogo.png'

class Header extends React.Component {
  render() {
    return (
      <div class="header">
        <ul>
          <li><img src={HeaderLogo} alt="logo" /></li>
          <li><a href="#">PHIM RẠP</a></li>
          <li><a href="#">CGV</a></li>
          <li><a href="#">THÀNH VIÊN</a></li>
          <li><a href="#">CULTUREPLEX</a></li>
        </ul>
      </div>
    );
  }
}

export default Header;